/**
 * Activate expression for email input
 * @version 0.0.0
 * @author Denis Chenu
 */
$(document).on("keyup change",".answer-item :email:not([onkeyup])",function(event){
    checkconditions($(this).val(), $(this).attr('name'), 'text', 'keyup')
});
